var http = require('http');
var through = require('through2');

var upperStream = through(transform);

var server = http.createServer(function (req, res)
{
	req.pipe(upperStream).pipe(res);
});

function transform(buffer, _, next)
{

	this.push(buffer.toString().toUpperCase());

	next();
}

server.listen(process.argv[2]);