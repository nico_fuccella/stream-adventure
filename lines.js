
var through = require('through2');
var split = require('split');

var transform = through(lines);

var lineUp = false;

function lines(buffer, _,next)
{
	var str = buffer.toString();


	if (lineUp)
	{
		str = str.toUpperCase();
	}
	else
	{
		str = str.toLowerCase();
	}

	str += '\n';

	lineUp = !lineUp;

	this.push(str);

	next();
}

process.stdin.pipe(split()).pipe(transform).pipe(process.stdout); 