var duplexer = require('duplexer2');
var spawn = require('child_process').spawn;

module.exports = function (cmd, args)
{
	var tmp = spawn(cmd, args);

	var stream = duplexer(tmp.stdin, tmp.stdout);

	return stream;
}