var trumpet = require('trumpet');
var through = require ('through2');
var fs = require('fs');

var tr = trumpet();
var stream = through(upperFunc);
var upper = tr.select('.loud').createStream();

function upperFunc(text, _, next)
{
	this.push(text.toString().toUpperCase());
	next();
}

upper.pipe(stream).pipe(upper);

process.stdin.pipe(tr).pipe(process.stdout);